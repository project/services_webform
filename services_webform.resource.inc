<?php

/**
 * Get a webform via its node ID.
 */
function _services_webform_retrieve($nid) {
  $output = $data = array();

  $node = node_load($nid);

  $output['nid'] = $node->nid;
  $output['title'] = $node->title;
  $output['fields'] = array();

  $query = 'SELECT
              webform_component.cid,
              webform_component.form_key AS name,
              webform_component.name AS label,
              webform_component.type,
              webform_component.value,
              webform_component.extra,
              webform_component.required
            FROM
              {webform} webform
              LEFT JOIN {webform_component} webform_component ON (webform.nid = webform_component.nid)
            WHERE
              webform.nid = :nid AND webform.status = :status
            ORDER BY
              webform_component.weight';

  $results = db_query($query, array(':nid' => $nid, ':status' => 1));
  $fields = $results->fetchAll();

  foreach ($fields as $field) {
    $extra = unserialize($field->extra);

    // Display only non-private fields.
    if ($extra['private'] === 0) {
      $data['extra'] = $extra;
      $data['cid'] = $field->cid;
      $data['label'] = $field->label;
      $data['description'] = $data['extra']['description'];
      $data['tag'] = 'input';
      $data['attributes'] = array();
      $data['attributes']['name'] = $field->name;
      $data['options'] = array();

      // Correct field type.
      switch ($field->type) {
        case 'textarea':
          $data['tag'] = 'textarea';
          break;

        case 'select':
          if ($data['extra']['aslist'] == 0) {
            if ($data['extra']['multiple'] == 0) {
              $data['tag'] = 'radio';
            }
            elseif ($data['extra']['multiple'] == 1) {
              $data['tag'] = 'checkbox';
            }
          }
          else {
            $data['tag'] = 'select';
          }
          break;

        default:
          $data['attributes']['title'] = $field->label;

          if ($field->type == 'textfield') {
            $data['attributes']['type'] = 'text';
          }
          else {
            $data['attributes']['type'] = $field->type;
          }
          break;
      }

      // Additional attributes.
      if (isset($data['extra']['placeholder'])) {
        $data['attributes']['placeholder'] = $data['extra']['placeholder'];
      }
      if (isset($data['extra']['maxlength']) && !empty($data['extra']['maxlength'])) {
        $data['attributes']['maxlength'] = $data['extra']['maxlength'];
      }
      if (isset($data['extra']['width']) && !empty($data['extra']['width'])) {
        $data['attributes']['width'] = $data['extra']['width'];
      }
      if (isset($data['extra']['min']) && !empty($data['extra']['min'])) {
        $data['attributes']['min'] = $data['extra']['min'];
      }
      if (isset($data['extra']['max']) && !empty($data['extra']['max'])) {
        $data['attributes']['max'] = $data['extra']['max'];
      }
      if (isset($data['extra']['step']) && !empty($data['extra']['step'])) {
        $data['attributes']['step'] = $data['extra']['step'];
      }
      if (isset($data['extra']['rows']) && !empty($data['extra']['rows'])) {
        $data['attributes']['rows'] = $data['extra']['rows'];
      }
      if (isset($data['extra']['cols']) && !empty($data['extra']['cols'])) {
        $data['attributes']['cols'] = $data['extra']['cols'];
      }
      if (isset($data['extra']['disabled']) && ($data['extra']['disabled'] == 1)) {
        $data['attributes']['disabled'] = 'true';
      }
      if (isset($data['extra']['readonly']) && ($data['extra']['readonly'] == 1)) {
        $data['attributes']['readonly'] = 'true';
      }
      if ($field->required == 1) {
        $data['attributes']['required'] = 'true';
      }
      if (isset($data['extra']['css_classes']) && !empty($data['extra']['css_classes'])) {
        $data['attributes']['class'] = $data['extra']['css_classes'];
      }
      if (isset($data['extra']['multiple']) && ($data['extra']['multiple'] == 1) && ($data['extra']['aslist'] == 1)) {
        $data['attributes']['multiple'] = 'multiple';
      }

      // Default value.
      $data['attributes']['value'] = $field->value;

      // Available options for SELECT, RADIO, or CHECKBOX field types.
      if (isset($data['extra']['items']) && !empty($data['extra']['items'])) {
        $items = explode("\r\n", $data['extra']['items']);
        $data['options'] = _services_webform_options($items);
      }

      // Field prefix.
      $data['prefix'] = '';
      if (isset($data['extra']['field_prefix'])) {
        $data['prefix'] = $data['extra']['field_prefix'];
      }

      // Field suffix.
      $data['suffix'] = '';
      if (isset($data['extra']['field_suffix'])) {
        $data['suffix'] = $data['extra']['field_suffix'];
      }

      // Wrapper classes.
      $data['wrapper_classes'] = $data['extra']['wrapper_classes'];

      // Remove 'extra' from output.
      unset($data['extra']);

      $output['fields'][] = $data;
    }
  }

  return $output;
}

/**
 * Callback function to convert OPTIONS string into an array.
 */
function _services_webform_options($items = array()) {
  $options = array();
  $option = array();
  foreach ($items as $item) {
    $option = explode('|', $item);
    if (is_array($option) && (count($option) == 2)) {
      $options[$option[0]] = $option[1];
    }
  }

  return $options;
}

/**
 * Save webform submission.
 */
function _services_webform_create($nid = 0, $submission = array()) {
  global $user;

  // Get the node ID of the created Webform and load the node.
  $node = node_load($nid);
  $uid = $user->uid;

  // Include files.
  module_load_include('inc', 'webform', 'webform.module');
  module_load_include('inc', 'webform', 'includes/webform.submissions');

  $data = $submission;

  // Arrange $data in the way Webform expects it.
  $data = _webform_client_form_submit_flatten($node, $data);
  $data = webform_submission_data($node, $data);

  $submission = (object) array(
    'nid' => $node->nid,
    'uid' => $user->uid,
    'submitted' => REQUEST_TIME,
    'remote_addr' => ip_address(),
    'is_draft' => FALSE,
    'data' => $data,
  );

  $sid = webform_submission_insert($node, $submission);

  return $sid;
}
